import java.util.Scanner;

//Liam Nunes
public class ShoppingCartManager {
    //print menu method
    public static void printMenu()
    {
        //System.out.println();
        System.out.println("MENU");
        System.out.println("a - Add item to cart");
        System.out.println("d - Remove item from cart");
        System.out.println("c - Change item quantity");
        System.out.println("i - Output items' descriptions");
        System.out.println("o - Output shopping cart");
        System.out.println("q - Quit");
        System.out.println();
        System.out.println("Choose an option:");
    }

    //main
    public static void main(String[] args){
        Scanner scnr = new Scanner(System.in);

        //data input for customer name and date
        System.out.println("Enter Customer's Name:");
        String custName = scnr.nextLine();
        System.out.println("Enter Today's Date:");
        String date = scnr.nextLine();
        System.out.println();
        System.out.println("Customer Name: " + custName);
        System.out.println("Today's Date: " + date);
        System.out.println();
        ShoppingCart myCart = new ShoppingCart(custName,date);

        //menu loop
        char menuChoice='q';
        boolean loopVal = true;
        while ( loopVal ){
            if (menuChoice == 'a' || menuChoice == 'd' || menuChoice == 'c' || menuChoice == 'i' || menuChoice == 'o' || menuChoice == 'q') {
                printMenu();
            }
            else{
                System.out.println("Choose an option:");
            }
            //user input section
            String userChoice = scnr.nextLine();
            menuChoice = userChoice.charAt(0);

            //add item to cart
            if ( menuChoice == 'a')
            {
                //get user input
                System.out.println("ADD ITEM TO CART");
                System.out.println("Enter the item name:");
                String tempItemName = scnr.nextLine();
                System.out.println("Enter the item description:");
                String tempItemDescription = scnr.nextLine();
                System.out.println("Enter the item price:");
                int tempItemPrice = scnr.nextInt();
                scnr.nextLine();
                System.out.println("Enter the item quantity:");
                int tempItemQuantity = scnr.nextInt();
                scnr.nextLine();

                //add to a item then add item to cart
                ItemToPurchase tempItem = new ItemToPurchase(tempItemName,tempItemDescription,tempItemPrice,tempItemQuantity);
                myCart.addItem(tempItem);
                System.out.println();

            }
            //remove item from cart
            else if ( menuChoice == 'd')
            {
                System.out.println("REMOVE ITEM FROM CART");
                System.out.println("Enter name of item to remove:");
                String itemToDelete = scnr.nextLine();
                myCart.removeItem(itemToDelete);
                System.out.println();

            }
            //char menuChoice;
            else if ( menuChoice == 'c')
            {
                //user input
                System.out.println("CHANGE ITEM QUANTITY");
                System.out.println("Enter the item name:");
                String itemToModify = scnr.nextLine();
                System.out.println("Enter the new quantity:");
                int newQuantity = scnr.nextInt();
                scnr.nextLine();

                //make item then modify cart
                ItemToPurchase itemNewQuantity = new ItemToPurchase();
                itemNewQuantity.setName(itemToModify);
                itemNewQuantity.setQuantity(newQuantity);
                myCart.modifyItem(itemNewQuantity);

                System.out.println();
            }
            //outputs shopping cart descriptions
            else if ( menuChoice == 'i')
            {
                System.out.println("OUTPUT ITEMS' DESCRIPTIONS");
                myCart.printDescriptions();
                System.out.println();
            }
            //outputs shopping cart
            else if ( menuChoice == 'o')
            {
                System.out.println("OUTPUT SHOPPING CART");
                myCart.printTotal();
            }
            //quits application
            else if ( menuChoice == 'q')
            {
                loopVal = false;
            }

        }
    }
}