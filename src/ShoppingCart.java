import java.util.ArrayList;

//Liam Nunes
public class ShoppingCart {
    //private variables
    private String customerName;
    private String currentDate;
    private ArrayList<ItemToPurchase> cartItems = new ArrayList<>();

    //constructor
    public ShoppingCart(){
        customerName = "none";
        currentDate = "January 1, 2016";
    }
    public ShoppingCart(String name , String date){
        customerName = name;
        currentDate = date;
    }

    //public methods
    //get cust name
    public String getCustomerName(){
        return customerName;
    }
    //get current date
    public String getDate(){
        return currentDate;
    }
    //add item
    public void addItem(ItemToPurchase item){
        cartItems.add(item);
    }
    //remove item
    public void removeItem(String itemName){
        int wasRemoved = 0;
        for(int i = 0 ; i < cartItems.size() ; i++){
            if(cartItems.get(i).getName().equals(itemName)){
                cartItems.remove(i);
                wasRemoved=1;
            }
        }
        if(wasRemoved == 0 ){
            System.out.println("Item not found in cart. Nothing removed.");
        }
    }
    //modify item
    public void modifyItem(ItemToPurchase newItem){
        int wasFound = 0;
        for(int i = 0 ; i < cartItems.size() ; i++){
            if(cartItems.get(i).getName().equals(newItem.getName())){
                //replace with parameters that are not default values
                if(newItem.getPrice() != 0){
                    cartItems.get(i).setPrice(newItem.getPrice());
                }
                if(newItem.getQuantity() != 0){
                    cartItems.get(i).setQuantity(newItem.getQuantity());
                }
                if(newItem.getDescription() != "none"){
                    cartItems.get(i).setDescription(newItem.getDescription());
                }
                wasFound=1;
            }
        }
        if(wasFound == 0 ){
            System.out.println("Item not found in cart. Nothing modified.");
        }
    }
    //get the number of items in cart
    public int getNumItemsInCart(){
        int itemsInCart=0;
        for(int i = 0 ; i < cartItems.size() ; i++){
            itemsInCart = itemsInCart + cartItems.get(i).getQuantity();
        }
        return itemsInCart;
    }
    //get the total cost of the cart
    public int getCostOfCart(){
        int runningTotal = 0;
        for(int i = 0 ; i < cartItems.size() ; i++){
            runningTotal = runningTotal + (cartItems.get(i).getPrice() * cartItems.get(i).getQuantity());
        }
        return runningTotal;
    }
    //prints the cart out (items quantities prices and total)
    public void printTotal(){
        System.out.println(customerName + "'s Shopping Cart - " + currentDate);
        System.out.println("Number of Items: " + getNumItemsInCart());
        System.out.println();
        if (cartItems.size() == 0){
            System.out.println("SHOPPING CART IS EMPTY");
        }
        for(int i = 0 ; i < cartItems.size() ; i++){
            cartItems.get(i).printItemCost();
        }
        System.out.println();
        System.out.println("Total: $" + getCostOfCart());
        System.out.println();
    }
    //prints descriptions of items in cart
    public void printDescriptions(){
        System.out.println(customerName + "'s Shopping Cart - " + currentDate);
        System.out.println();
        System.out.println("Item Descriptions");
        for(int i = 0 ; i < cartItems.size() ; i++){
            cartItems.get(i).printItemDescription();
        }
    }
}