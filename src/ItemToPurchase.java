//Liam Nunes
public class ItemToPurchase {
    //private variables
    private int itemPrice;
    private String itemName;
    private String itemDescription;
    private int itemQuantity;
    //default constructor
    public ItemToPurchase(){
        itemPrice = 0;
        itemQuantity = 0;
        itemName = "none";
        itemDescription = "none";
    }
    //paramiterized constructor
    public ItemToPurchase(String name , String description , int price , int quantity){
        itemPrice = price;
        itemQuantity = quantity;
        itemName = name;
        itemDescription = description;
    }

    //get and set name methods
    public void setName(String newName){
        itemName = newName;
    }
    public String getName(){
        return itemName;
    }

    //get and set price
    public void setPrice(int newPrice){
        itemPrice = newPrice;
    }
    public int getPrice(){
        return itemPrice;
    }

    //get and set price
    public void setQuantity(int newQuantity){
        itemQuantity = newQuantity;
    }
    public int getQuantity(){
        return itemQuantity;
    }

    //get and set description
    public void setDescription(String newDescription){
        itemDescription = newDescription;
    }
    public String getDescription(){
        return itemDescription;
    }

    //print statments
    public void printItemCost() {
        System.out.println(itemName + " " + itemQuantity + " @ $" + itemPrice + " = $" + (itemPrice * itemQuantity));
    }
    public void printItemDescription() {
        System.out.println(itemName + ": " + itemDescription);
    }
}
